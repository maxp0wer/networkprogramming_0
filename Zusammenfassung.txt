Serializer
	CLI -> Common Intermediate Language
		Objektorientierte Assemblersprache.
		Bei der Interpretation von C# code wird entgegen anderer Hochsprachen wie C++
		kein Assembler generiert sondern CLI code.
		Der CLI code wird dann auf der spezifischen Zielplatform in den jeweilig passenden
		Assembler übersetzt.

		Die erzeugten Daten sind ohne Hilfsmittel nicht lesbar.
		
		Binary Formatter:
			Eine möglichkeit der Serialisierung von Objekten.
			Der Binary Formatter übersetzt Objekte in.
			Ist sehr langsam sowohl bei der Deserialisierung und Serialisierung von kleinen
			und großen Datenmengen.
			Ist Teil der StandardLibrary und ist Teil der Mono implementation.	
			
		Google Protocol Buffers:
			Ist der schnellste Serializer der zurzeit genutzt wird.
			Er ist sowohl bei kleinen als auch großen Datenmengen sehr schnell.
		
		Message Pack:
			Ist bei kleinen Datenmengen sehr schnell aber wird bei großen 
			Datenmengen sehr schnell langsam.,
	JSON
	
		Javascript Object Notation
		
		Ist bei großen Datenmengen sehr langsam da früher oder später die C# string
		operationen die genutzt werden zu langsam sind.
		Bei kleinen Datenmengen aber relativ schnell und hat den großen Vorteil,
		dass es gut lesbar ist.
		
		Für JSON wird eine externe Library gebraucht.
		Beste Möglichkeit ist hier Newtonsoft JSON.
	
		class Foo
		{
			int Bla 	= 10;
			int Blubb 	= 5;
		}
	
		{
			"KEY":"VALUE",
			"KEY":"VALUE",
			"ARRAY":
			[
				{ "KEY":"Value" }
			]
		}
	
	XML
		Extended Markup Language
		
		<tag />
		<tag>
			Der Content zum Tag.
		</tag>
		<tag attribut="wertdesAttributs">
			Der Content hier rein.
		</tag>
		
		Ist sehr übersichtlich, da wir die Möglichkeit haben Werte in Tags zusammenzufassen
		und Tags zusätzlich noch Attribute geben können.
		Ist aber sowohl bei kleinen als auch großen Datenmengen sehr langsam.
		XML kann standardmäßig über Linq angesprochen werden insofern ist das auslesen
		der Daten angenehm und schnell.
		Ist auch Teil der StandardLibary und so ohne externe Tools, 
		auch in Mono, verwendbar.
		
Erklärung von Bits:
		Bits haben grundsätzlich keine Bedeutung für den Rechner. Nur dadurch dass wir Bits bestimmten
		Regeln untewerfen haben wir die Möglichkeit informationen zu extrahieren.
		Die Regel die auf Bits angewendet wird ist wie folgt:
			Von Rechts nach links gelesen werden Bits in Power of 2 Werte zugewiesen.
			Bits 		0 0 0 0
			Wertigkeit	8 4 2 1	
			
			Bit wert: 	0 1 0 1
			Dezimalwert: 5	
		
		Der Big oder Little Endian stellt die Byte order dar.
		Einmal von links nach rechts gelesen (Big)
		und von rechts nach links gelesen (Little).
		
		Bitfield: 0 0 0 0 0 0 1 1	 
		
		Setting Foo: 0 0 0 0 0 0 1 0
		Setting Bar: 0 0 0 0 0 0 0 1
		
		Bitfield  | Setting Bar
		Bitfield  & Setting Bar
		~ Dreht die wertigkeit aller bytes um
		byte ~0 wird zu byte max value 
		
Lambda Expressions / Anonyme Methoden:
		Lambda expressions sind eine kurzschreibweise von Methoden.
		Sie können inline deklariert werden.
		Sie können immer nur eine Anweisung enthalten.
		Anonyme Funktionen sind wie lambda expressions nur dass
		sie auch einen Body anbieten in dem mehr als eine Anweisung stehen kann.

		private void Bar(_methode)
		{
			
		}
		Bar((_bool, _string) => true);
		
		private bool Foo(bool _bool, string _string)
		{
			
		}
		
		(_bool, _string) => { return true; }
		(_bool, _string) => true;

Dictionaries Collections:
	- 	Ist eine Collection vergleichbar mit einer List.
		Ein Dictionary ist immer aus Key-Value pairs aufgebaut.
		Ein Key steht dabei für einen Value und ein Key darf nur einmal
		im Dictionary vorkommen.
		
		Dictionaries sind schnell beim Zugriff aber eher langsam beim hinzufügen/entfernen/manipulieren
		von Elementen.
		Macht vor allem Sinn wenn die Elemente des D schon vor der Laufzeit festgelegt wurden ist
		aber auch zur Laufzeit gut verwendbar.
		
		Dictionary[PlayerName]

Linq:
	
		//var address = addresses.FirstOrDefault(TestElement);
		//var address = addresses.FirstOrDefault((_address) => _address.AddressFamily == AddressFamily.InterNetwork);

		addresses.Any((_address) => _address.AddressFamily == AddressFamily.InterNetwork);
		addresses.All((_address) => _address.AddressFamily == AddressFamily.InterNetwork);

		addresses.ToList();
		addresses.ToArray();

		//Das erste element dass die Bedingung erfüllt.
		//Mindestens ein element muss die Bedingung erfüllen.
		addresses.First();
		//Das erste element oder default(element).
		//Wirft keine exception wenn das element nicht existiert.
		addresses.FirstOrDefault((_address) => _address.AddressFamily == AddressFamily.InterNetwork);
		//Eine liste an elementen die Bedingung erfüllt.
		addresses.Where(_o => _o.AddressFamily == AddressFamily.InterNetwork);
		//Extrahiert felder aus _o und gibt sie in einer neuen collection zurück.
		addresses.Select(_o => _o.GetType());
		//Extrahiert alle listen elemente aus collection und gibt sie zusammengefasst in
		//collection zurück.
		addresses.SelectMany();
		
		//Noch erklärung schreiben!
		addresses.Single();
		addresses.Sum(_o => _o.gameCount);
		addresses.Count();
		addresses.Skip(5);
		addresses.Take(5);
		addresses.OrderBy(_o => _o.Score).Take(5);
		addresses.Cast<object>();
		
		class Test
		{
			int gameCount;
		}
﻿using ProtoBuf;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace Protocol
{
    public class Prot
    {
        public const int PORT = 25565;

        public const int PACKET_SIZE = 2048;

        public const string MSG_DELIMITER = "%";

        public static void WriteObjectToSocket(Socket _targetSocket, ProtMessage _data)
        {
            using (var networkStream = new NetworkStream(_targetSocket))
            {
                Serializer.SerializeWithLengthPrefix<ProtMessage>(networkStream, _data, PrefixStyle.Fixed32);
            }
        }

        public static void ReadObjectFromSocket(    Socket _targetSocket, 
                                                    System.Action<ProtMessage> _onMessageReceived)
        {
            using (var networkStream = new NetworkStream(_targetSocket))
            {
                var result = Serializer.DeserializeWithLengthPrefix<ProtMessage>(networkStream, PrefixStyle.Fixed32);
                _onMessageReceived(result);
            }
        }
    }

    [ProtoContract]
    [ProtoInclude(3, typeof(UserMessage))]
    [ProtoInclude(4, typeof(UserNameChanged))]
    public class ProtMessage
    {
        [ProtoMember(1)]
        public EMessageType Type;
        [ProtoMember(2)]
        public string Message;

        public ProtMessage() { }
        public ProtMessage(EMessageType _typeMask, string _message)
        {
            Type = _typeMask;
            Message = _message;
        }
    }

    [ProtoContract]
    public class UserMessage : ProtMessage
    {
        [ProtoMember(1)]
        public string UserName;
        [ProtoMember(2)]
        public int UserColor;

        public UserMessage() { }
        public UserMessage(string _userName, int _userColor, string _message) 
            : base(EMessageType.USER_MESSAGE, _message)
        {
            UserName = _userName;
            UserColor = _userColor;
        }
    }

    [ProtoContract]
    public class UserNameChanged : ProtMessage
    {
        [ProtoMember(1)]
        public string OldName;
        [ProtoMember(2)]     
        public string NewName;

        public UserNameChanged() { }
        public UserNameChanged(string _oldName, string _newName)
            : base(EMessageType.USER_NAME_CHANGED, "name changed")
        {
            OldName = _oldName;
            NewName = _newName;
        }
    }

    [ProtoContract]
    [System.Flags]
    public enum EMessageType
    {
        [ProtoMember(1)]
        PING                = 0x01,
        [ProtoMember(2)]
        USER_MESSAGE        = 0x02,
        [ProtoMember(3)]
        USER_NAME_CHANGED   = 0x04,
        [ProtoMember(4)]
        QUIT                = 0x08
    }

    /*
     * USER
     * MESSAGE
     * WHISPER
     * LOGIN
     * 
     * USER | MESSAGE
     * USER | MESSAGE | WHISPER
     * USER | LOGIN
     * */
}

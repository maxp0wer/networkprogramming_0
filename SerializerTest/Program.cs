﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProtoBuf;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;

namespace SerializerTest
{
    public class Program
    {
        private static Stopwatch stopWatch;

        public static void Main(string[] args)
        {
            stopWatch = new Stopwatch();

            stopWatch.Start();
            XMLSerializer();
            System.Console.WriteLine("XML Serializer took: " + stopWatch.ElapsedMilliseconds + "ms");

            stopWatch.Restart();
            BinarySerializer();
            System.Console.WriteLine("Binary Serializer took: " + stopWatch.ElapsedMilliseconds + "ms");

            stopWatch.Restart();
            JSONSerializer();
            System.Console.WriteLine("JSON Serializer took: " + stopWatch.ElapsedMilliseconds + "ms");

            stopWatch.Restart();
            ProtoBufSerializer();
            System.Console.WriteLine("ProtoBuf Serializer took: " + stopWatch.ElapsedMilliseconds + "ms");
        }

        private static void XMLSerializer()
        {
            var seri = new XmlSerializer(typeof(Settings));
            var settings = Settings.CreateDefault();

            try
            {
                File.Delete("settings.xml");
                using (var stream = File.OpenWrite("settings.xml"))
                {
                    seri.Serialize(stream, settings);
                }
            }
            catch (System.Exception _e)
            {
                System.Console.WriteLine("Failed to serialize xml data! " + _e.Message);
            }

            try
            {
                using (var stream = File.OpenRead("settings.xml"))
                {
                    var setObj = seri.Deserialize(stream) as Settings;
                    System.Console.WriteLine(setObj);
                }
            } catch(System.Exception _e)
            {
                System.Console.WriteLine("Failed to deserialize xml data! " + _e.Message);
            }
        }

        private static void BinarySerializer()
        {
            var settings = Settings.CreateDefault();
            BinaryFormatter bF = new BinaryFormatter();

            using (var stream = File.OpenWrite("test.bin"))
            {
                bF.Serialize(stream, settings);
            }

            Settings loadedSettings;
            //try
            //{
            //    stream = File.OpenRead("test.bin");
            //    loadedSettings = (Settings)bF.Deserialize(stream);
            //}
            //catch { }
            //finally
            //{
            //    stream?.Close();
            //}

            using (var stream = File.OpenRead("test.bin"))
            {
                loadedSettings = (Settings)bF.Deserialize(stream);
            }

            System.Console.WriteLine(loadedSettings);
        }

        private static void JSONSerializer()
        {
            var jsonString = JsonConvert.SerializeObject(Settings.CreateDefault());
            File.Delete("settings.json");
            using (var stream = File.OpenWrite("settings.json"))
            {
                var data = Encoding.UTF8.GetBytes(jsonString);
                stream.Write(data, 0, data.Length);
            }
            File.WriteAllText("settings.json", jsonString);
            var jsonDesString = File.ReadAllText("settings.json");
            var setObj = JsonConvert.DeserializeObject<Settings>(jsonDesString);
            System.Console.WriteLine(setObj.GetType().Name);
        }

        private static void ProtoBufSerializer()
        {
            var settings = new ProtoBufSettings()
            {
                Score = 100,
                Playername = "Pete"
            };
            using (var stream = File.OpenWrite("settings.proto.bin"))
            {
                Serializer.SerializeWithLengthPrefix<ProtoBufSettings>(stream, settings, PrefixStyle.Fixed32);
            }

            using (var stream = File.OpenRead("settings.proto.bin"))
            {
                var setObj = Serializer.DeserializeWithLengthPrefix<ProtoBufSettings>(stream, PrefixStyle.Fixed32);

                System.Console.WriteLine("Playername: " + setObj.Playername);
                System.Console.WriteLine("Score: " + setObj.Score);
            }
        }

        private T Foo<T, O>(T _t) where T : O, new()
        {
            return new T();
        }

        //private class Vector<T>
        //{
        //    public T X;
        //    public T Y;
        //}
    }

    [System.Serializable]
    public class Settings
    {
        public int Score;
        public string Playername;

        public static Settings CreateDefault()
        {
            return new Settings() { Score = 100, Playername = "Foobar" };
        }

        public override string ToString()
        {
            return 
$@"Playername: {Playername}
Score: {Score}";
        }
    }

    [ProtoContract]
    [ProtoInclude(3, typeof(SomeOtherSettingsClass))]
    public class ProtoBufSettings
    {
        [ProtoMember(1)]
        public int Score;
        [ProtoMember(2)]
        public string Playername;
    }

    [ProtoContract]
    public class SomeOtherSettingsClass : ProtoBufSettings
    {

    }
}

﻿using System.Net;

using Console = System.Console;

namespace DNSResolverTool
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            Console.WriteLine("***** DNS Resolve Tool! *****");
            do
            {
                string host = ReadHostInput();
                IPAddress[] addresses = FetchHostInfo(host);
                if(addresses == null)
                {
                    Console.WriteLine($"Failed to resolve hostname: {host}");
                }
                else
                {
                    PrintHostInfo(addresses);
                }
            } while (PromptForContinue());
        }

        private string ReadHostInput()
        {
            var hostInput = "";
            while (string.IsNullOrWhiteSpace(hostInput))
            {
                Console.WriteLine();
                Console.Write("Enter hostname: ");
                hostInput = Console.ReadLine();
            }
            return hostInput;
        }

        private bool PromptForContinue()
        {
            System.ConsoleKeyInfo value = new System.ConsoleKeyInfo();
            while ( value.Key != System.ConsoleKey.N && 
                    value.Key != System.ConsoleKey.Y)
            {
                Console.WriteLine();
                Console.Write("Do you want to continue?(y/n): ");
                value = Console.ReadKey();
            }
            return (value.Key == System.ConsoleKey.Y);
        }

        private IPAddress[] FetchHostInfo(string _hostName)
        {
            try
            {

                //hier passiert die magie!
                //der hostname wird an den zuständigen DNS server übermittelt,
                //wenn der host gefunden wurde gibt der DNS server die jeweiligen IP-Addressen
                //zurück.
                //Aufpassen, denn hier wird Netzwerkcode ausgeführt! 
                return Dns.GetHostAddresses(_hostName);
            }
            catch
            {
                return null;
            }
        }

        private void PrintHostInfo(IPAddress[] _adresses)
        {
            Console.WriteLine("Resolved host name to: ");
            Console.WriteLine("- " + string.Join<IPAddress>("\n- ", _adresses));
        }
    }
}

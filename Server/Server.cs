﻿using Protocol;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Console = System.Console;

namespace Server
{
    public class Server
    {
        private const int MAX_PENDING_CONNECTIONS = 10;

        //Socket ist unsere programmatische Schnittstelle zum Netzwerk.
        //Basis des Sockets ist ein Networkstream der sich grundsätzlich gleich verhält wie jede
        //andere Form von stream(File-/MemoryStream).
        //Ein Socket wird immer auf einen bestimmten Port gebindet auf den er hört.
        private Socket m_serverSocket;

        private bool m_isListening = true;

        private List<ClientServant> Servants
        {
            get
            {
                lock(m_servantLock)
                {
                    return m_servants;
                }
            }
        }
        private readonly object m_servantLock = new object();
        private List<ClientServant> m_servants = new List<ClientServant>();

        private Dictionary<EMessageType, System.Action<ClientServant, ProtMessage>> m_netCommands;

        public static void Main(string[] args)
        {
            Console.WriteLine("***** Simple Socket Server! *****");
            var server = new Server();
            server.Start();
            server.Run();
            server.Dispose();
        }

        private void Console_CancelKeyPress(object sender, System.ConsoleCancelEventArgs e)
        {
            Console.WriteLine("Exiting...");
            Dispose();
        }

        private void InitializeNetCommandMap()
        {
            m_netCommands.Add(EMessageType.PING, OnPingReceived);
            m_netCommands.Add(EMessageType.USER_MESSAGE, OnUserMessageReceived);
            m_netCommands.Add(EMessageType.USER_NAME_CHANGED, OnUserNameChanged);
            m_netCommands.Add(EMessageType.QUIT, OnUserQuitReceived);
        }

        public void Start()
        {
            m_netCommands = new Dictionary<EMessageType, System.Action<ClientServant, ProtMessage>>();
            InitializeNetCommandMap();

            Console.CancelKeyPress += Console_CancelKeyPress;
            Console.WriteLine("Binding socket on " + IPAddress.Any + ":" + Prot.PORT);
            m_serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_serverSocket.Bind(new IPEndPoint(IPAddress.Any, Prot.PORT));
            m_serverSocket.Listen(MAX_PENDING_CONNECTIONS);
            Console.WriteLine("Awaiting client connections...");
        }

        private void Run()
        {
            try
            {
                while (m_isListening)
                {
                    var clientSocket = m_serverSocket.Accept();
                    Console.WriteLine("Client connected from remote end point: " + clientSocket.RemoteEndPoint);
                    var servant = new ClientServant();
                    servant.Start(this, clientSocket);
                    m_servants.Add(servant);
                }
            }
            catch (System.Exception _e)
            {
                Console.WriteLine("Error occurred while receiving client message: " + _e.Message);
            }
        }

        public void OnMessageReceived(ClientServant _servant, ProtMessage _data)
        {
            if(_data == null)
            {
                Console.WriteLine("Invalid client message received, dropping...");
                return;
            }

            if(m_netCommands.ContainsKey(_data.Type))
            {
                m_netCommands[_data.Type](_servant, _data);
                return;
            }

            Console.WriteLine("Message received from client: " + _data.Message);
        }

        public void OnClientDisconnected(ClientServant _servant)
        {
            Console.WriteLine("Client servant disconnected: " + _servant);
            m_servants.Remove(_servant);
        }

        public void Broadcast(ProtMessage _message)
        {
            foreach (var servant in Servants)
                servant.Send(_message);
        }

        public void Dispose()
        {
            if(m_serverSocket != null && m_serverSocket.Connected)
                m_serverSocket.Close();
        }

        private void OnPingReceived(ClientServant _servant, ProtMessage _ping)
        {
            Console.WriteLine("Client ping received, sending hearty pong!");
            _servant.Send(new ProtMessage(EMessageType.PING, "pong!"));
        }

        private void OnUserMessageReceived(ClientServant _servant, ProtMessage _userMessage)
        {
            var uMsg = _userMessage as UserMessage;
            if (uMsg == null)
                return;

            System.Console.WriteLine($"User message from {uMsg.UserName}: {uMsg.Message}");
            Broadcast(_userMessage);
        }

        private void OnUserNameChanged(ClientServant _servant, ProtMessage _changeMessage)
        {
            var userChangeMessage = _changeMessage as UserNameChanged;
            if(userChangeMessage == null)
                return;
            System.Console.WriteLine($"{userChangeMessage.OldName} changed name to {userChangeMessage.NewName}");
            Broadcast(_changeMessage);
        }

        private void OnUserQuitReceived(ClientServant _servant, ProtMessage _quitMessage)
        {
            System.Console.WriteLine("Client has sent quit signal! Disconnecting...");
            _servant.Dispose();
        }
    }
}

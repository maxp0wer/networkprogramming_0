﻿using Protocol;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Console = System.Console;

namespace Client
{
    public class Client
    {
        private Socket m_clientSocket;
        private Thread m_clientListenerThread;

        private bool m_isListening = true;
        private string m_userName = "Guest";
        private System.ConsoleColor m_userColor;

        //Die stopwatch misst in einem separaten thread die vergangene zeit.
        //Sie speichert die vergangenen millisekunden, die wir nutzen um die Zeit eines pings zu messen.
        private Stopwatch m_pingTimer = new Stopwatch();

        //Dictionaries sind key value pair collections. Jeder key darf nur einmal im Dictionary vorkommen.
        //Wenn versucht wird einen bereits vorhandenen key hinzuzufügen wirft das Dictionary 
        //eine InvalidOperationException.
        //Das Dictionary ist sehr schnell beim Zugriff auf einzelne elemente aber weniger
        //beim modifizieren der Collection(elemente added, removen).
        //Da in unserem Fall der zugriff schnell sein muss und wir nicht zur laufzeit elemente hinzufügen,
        //ist es hier ideal.
        private Dictionary<string, System.Action<string[]>> m_commandMap;
        private Dictionary<EMessageType, System.Action<ProtMessage>> m_netCommands;

        public static void Main(string[] args)
        {
            var client = new Client();
            client.Run();
            client.Dispose();
        }

        private void Run()
        {
            m_commandMap = new Dictionary<string, System.Action<string[]>>();
            m_netCommands = new Dictionary<EMessageType, System.Action<ProtMessage>>();
            InitializeNetCommandMap();
            InitializeCommandMap();

            Console.WriteLine("***** Simple Socket Client *****");
            do
            {
                string host = ReadHostInput();
                IPAddress[] addresses = FetchHostInfo(host);

                //var address = addresses.FirstOrDefault(TestElement);
                //var address = addresses.FirstOrDefault((_address) => _address.AddressFamily == AddressFamily.InterNetwork);

                addresses.Any((_address) => _address.AddressFamily == AddressFamily.InterNetwork);
                addresses.All((_address) => _address.AddressFamily == AddressFamily.InterNetwork);

                addresses.ToList();
                addresses.ToArray();

                //Das erste element dass die Bedingung erfüllt.
                //Mindestens ein element muss die Bedingung erfüllen.
                addresses.First();
                //Das erste element oder default(element).
                //Wirft keine exception wenn das element nicht existiert.
                addresses.FirstOrDefault((_address) => _address.AddressFamily == AddressFamily.InterNetwork);
                //Eine liste an elementen die Bedingung erfüllt.
                addresses.Where(_o => _o.AddressFamily == AddressFamily.InterNetwork);
                //Extrahiert felder aus _o und gibt sie in einer neuen collection zurück.
                addresses.Select(_o => _o.GetType());
                //Extrahiert alle listen elemente aus collection und gibt sie zusammengefasst in
                //collection zurück.
                addresses.SelectMany();
                addresses.Single();
                addresses.Sum(_o => _o.gameCount);
                addresses.Count();
                addresses.Skip(5);
                addresses.Take(5);
                addresses.OrderBy(_o => _o.Score).Take(5);
                addresses.Cast<object>();
                class Test
                {
                    int gameCount;
                }

                IPAddress add = null;
                foreach (var address in addresses)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        add = address;
                        break;
                    }
                }

                if (add == null)
                {
                    Console.WriteLine($"Failed to resolve hostname: {host}");
                }
                else
                {
                    Start(add);
                    ReadCommandInput();
                }
            } while (PromptForContinue());
        }

        private bool TestElement(IPAddress _address)
        {
            return _address.AddressFamily == AddressFamily.InterNetwork;
        }

        //private void Send(string _message)
        //{
        //    try
        //    {
        //        var bytes = Encoding.UTF8.GetBytes(_message.Trim() + Prot.MSG_DELIMITER);
        //        m_clientSocket.Send(bytes, 0, bytes.Length, SocketFlags.None);
        //    }
        //    catch (System.Exception _e)
        //    {
        //        Console.WriteLine("Failed to send message: " + _e.Message);
        //    }
        //}

        private void InitializeCommandMap()
        {
            m_commandMap.Add("/exit",       (_o) => Dispose());
            m_commandMap.Add("/setname",    OnSetUserName);
            m_commandMap.Add("/setcolor",   OnSetUserColor);
            m_commandMap.Add("/ping",       (_o) => Ping());
            m_commandMap.Add("/quit",       OnQuit);
        }

        private void InitializeNetCommandMap()
        {
            m_netCommands.Add(EMessageType.USER_MESSAGE, OnUserMessageReceived);
            m_netCommands.Add(EMessageType.PING, OnPingReceived);
            m_netCommands.Add(EMessageType.USER_NAME_CHANGED, OnUserNameChanged);
        }

        private void Ping()
        {
            m_pingTimer.Start();
            Send(new ProtMessage(EMessageType.PING, "ping!"));
        }

        private void Send(ProtMessage _data)
        {
            try
            {
                Prot.WriteObjectToSocket(m_clientSocket, _data);
            }
            catch (System.Exception _e)
            {
                System.Console.WriteLine(
                    $"Error occurred while serializing message to socket({_e.GetType()}): {_e.Message}");
                Dispose();
            }
        }

        private void Start(IPAddress _hostAddress)
        {
            try
            {
                //1. AddressFamily bezeichnet die Art von vermittlungsprotokoll die wir nutzen möchten.
                //2. Socket type beschreibt die zugrundeliegende socket-art.
                //3. Protokolltyp entweder UDP oder TCP je nachdem was gebraucht wird.
                m_clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //Ip end point ist nichts weiteres als eine wrapper klasse die IP-addresse und Port zusammenfasst.
                IPEndPoint endpoint = new IPEndPoint(_hostAddress, Prot.PORT);
                m_clientSocket.Connect(endpoint);
                if (m_clientSocket.Connected)
                {
                    Console.WriteLine("Client successfully connected to " + endpoint);
                    m_clientListenerThread = new Thread(RunListener);
                    m_clientListenerThread.Start();
                }
            }
            catch (System.Exception _e)
            {
                Console.WriteLine($"Client failed to connect({_e.GetType().Name}): {_e.Message}");
            }
        }

        //private void RunListener()
        //{
        //    byte[] data = new byte[Prot.PACKET_SIZE];
        //    string message = "";
        //    try
        //    {
        //        while (m_clientSocket.Receive(data, 0, data.Length, SocketFlags.None) > 0)
        //        {
        //            message += Encoding.UTF8.GetString(data).Replace("\0", "");

        //            if (message.Contains(Prot.MSG_DELIMITER))
        //            {
        //                OnMessageReceived(message.Replace(Prot.MSG_DELIMITER, ""));
        //                message = "";
        //            }
        //        }
        //    }
        //    catch { }
        //}

        private void ReadCommandInput()
        { 
            while(m_isListening)
            {
                var input = System.Console.ReadLine();
                string[] args = input.GetArgs();
                string cmd = args.Length > 0 ? args[0] : input; 

                if (!m_commandMap.ContainsKey(cmd))
                    Send(new UserMessage(m_userName, (int)m_userColor, input));
                else m_commandMap[cmd](args);
            }
        }

        private void RunListener()
        {
            try
            { 
                while (m_isListening)
                {
                    Prot.ReadObjectFromSocket(m_clientSocket, OnMessageReceived);
                }
            }
            catch (System.Exception _e)
            {
                System.Console.WriteLine(
                   $"Error occurred while deserializing message from socket({_e.GetType()}): {_e.Message}");
                Dispose();
            }
        }

        private void Dispose()
        {
            if(m_pingTimer.IsRunning)
            {
                m_pingTimer.Stop();
            }
            m_isListening = false;
            if (m_clientSocket != null && m_clientSocket.Connected)
                m_clientSocket.Close();
        }

        private string ReadHostInput()
        {
            var hostInput = "";
            while (string.IsNullOrWhiteSpace(hostInput))
            {
                Console.WriteLine();
                Console.Write("Enter hostname: ");
                hostInput = Console.ReadLine();
            }
            return hostInput;
        }

        private bool PromptForContinue()
        {
            System.ConsoleKeyInfo value = new System.ConsoleKeyInfo();
            while (value.Key != System.ConsoleKey.N &&
                    value.Key != System.ConsoleKey.Y)
            {
                Console.WriteLine();
                Console.Write("Do you want to reconnect?(y/n): ");
                value = Console.ReadKey();
            }
            return (value.Key == System.ConsoleKey.Y);
        }

        private IPAddress[] FetchHostInfo(string _hostName)
        {
            try
            {
                //hier passiert die magie!
                //der hostname wird an den zuständigen DNS server übermittelt,
                //wenn der host gefunden wurde gibt der DNS server die jeweiligen IP-Addressen
                //zurück.
                //Aufpassen, denn hier wird Netzwerkcode ausgeführt! 
                return Dns.GetHostAddresses(_hostName);
            }
            catch
            {
                return null;
            }
        }

        private void OnSetUserName(string[] _args)
        {
            if(_args.Length != 2)
            {
                System.Console.WriteLine("Expected: /setname (Name)");
                return;
            }

            Send(new UserNameChanged(m_userName, _args[1]));
            m_userName = _args[1];
        }

        private void OnSetUserColor(string[] _args)
        {
            if (_args.Length != 2)
            {
                string[] possibleCols = Enum.GetNames(typeof(System.ConsoleColor));
                System.Console.WriteLine("Expected: /setcolor ( " + string.Join(",", possibleCols) + ")");
                return;
            }

            System.ConsoleColor col;
            if(!Enum.TryParse<System.ConsoleColor>(_args[1], out col))
            {
                string[] possibleCols = Enum.GetNames(typeof(System.ConsoleColor));
                System.Console.WriteLine("Expected: /setcolor ( " + string.Join(",", possibleCols) + ")");
                return;
            }

            System.Console.WriteLine($"Changed message text color from {m_userColor} to {col}");
            m_userColor = col;
        }

        private void OnQuit(string[] _args)
        {
            Send(new ProtMessage(EMessageType.QUIT, "quit"));
        }

        private void OnMessageReceived(ProtMessage _data)
        {
            if (_data == null)
            {
                Console.WriteLine("Invalid server message received, exiting...");
                Dispose();
                return;
            }

            if(m_netCommands.ContainsKey(_data.Type))
            {
                m_netCommands[_data.Type](_data);
                return;
            }

            Console.WriteLine("Server message received: " + _data.Message);
        }

        private void OnUserMessageReceived(ProtMessage _data)
        {
            var uObj = _data as UserMessage;
            if (uObj == null)
                return;

            Console.Beep();
            System.Console.ForegroundColor = (System.ConsoleColor)uObj.UserColor;
            System.Console.WriteLine($"[{uObj.UserName}] {uObj.Message}");
            System.Console.ForegroundColor = System.ConsoleColor.White;
        }

        private void OnPingReceived(ProtMessage _data)
        {
            Console.WriteLine($"Server pong received, took {m_pingTimer.ElapsedMilliseconds}ms");
            m_pingTimer.Reset();
        }

        private void OnUserNameChanged(ProtMessage _data)
        {
            var userChangedMessage = _data as UserNameChanged;
            if (userChangedMessage == null)
                return;
            System.Console.WriteLine($"{userChangedMessage.OldName} is now known as {userChangedMessage.NewName}");
        }
    }

    static class Extension
    {
        public static string[] GetArgs(this string _value)
        {
            return _value.Split(' ');
        }
    }
}
